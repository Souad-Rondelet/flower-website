<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FlowersController extends AbstractController
{
    /**
     * @Route("/redflower", name="redflower_index")
     */
    public function index()
    {
        return $this->render('flowers/redflower.html.twig');
   
    }
    /**
     * @Route("/yellowflower", name = "yellowflower_index")
     */
    public function yellowFlower(){
        return $this->render('flowers/yellowsfl.html.twig');
    }

    /**
     * @Route("/pink-flower", name="pinkflower_index")
     */

     public function pinkflower(){
         return $this->render('flowers/pinkflower.html.twig');
     }
}
