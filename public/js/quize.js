//step1

const quizImage = document.getElementById("quizImage");
const quizQuestions = document.getElementById("quizQuestions");
const ul = document.getElementById("ul");
const option1 = document.getElementById("option1");
const option2 = document.getElementById("option2");
const option3 = document.getElementById("option3");
const option4 = document.getElementById("option4");

const buttonn = document.getElementById("btn");
const scoreCard = document.getElementById("scoreCard");

//step2
let quiz = {


    questions : [
        {  question: "Tulips are native to the ?",
        image: "../test-image/tulips.jpg",
        //property contain a table of strings 
        options: ["Netherlands", "USA", "south-central Asia", "Egypt"],
        answer: 3},
        {
            question: "The outermost part of a flower is called the ..?",
            image: "image/outermostflower.jpg",
            options: ["Stigma", "Anther", "Petals", "Calyx"],
            answer: 4   
        },
        {
            question: "What do you think this flower is called, knowing that its root is used as a spice ?",
            image: "image/ginger-4949735_1280.jpg",
            options: ["Ginger", "Lilac", "Freesia", "Peony"],
            answer: 1 
        },
        {
            question: "what's this tropical flower called?",
            image: "image/heliconia-1586256_1280.jpg",
            options: ["Sunflower", "Bird of paradise", "Lilac", "stock"],
            answer: 2 
        },
        {
            question: "This flower's name is often preceded by the word \"black.\" What is it called?",
            image: "image/dahlia-3521789_1280.jpg",
            options: ["Dahlia", "Lavender", "Freesia", "Phlox"],
            answer: 1
        },
        {
            question: "What is the most popular cut flower?",
            image: "image/bouquetrr.jpg",
            options: ["Carnation", "Daisy", "Chrysanthemum", "Rose"],
            answer: 4
        },
        {
            question: " Which word from the French “boschet” was introduced to English by Lady Mary Montague in 1716?",
            image: "image/lom.jpg",
            options: ["Bouvardia", "Bouquet", "Boutonniere", "Basket"],
            answer: 2
        },
        {
            question: "Which style of design was originally used by upper class members to show their opulence and wealth?",
            image: "image/living-room-581073_640.jpg",
            options: ["Flemish", "English Garden", "American Colonial", "Victorian"],
            answer: 4  
        },
        {
            question: " What flower is this? The genus includes over 200 species, with centers of diversity in North and South America.",
            image: "image/lupine-1488942_640.jpg",
            options: ["Clematis", "Lupine", "Liriope", "Common Foxglove"],
            answer: 2
        },
        {
            question: "What flower is this? They grow naturally throughout the northern hemisphere. One of our favorites in the garden.",
            image: "image/flower-2575662_640.jpg",
            options: ["Allium", "Astilbe", "Delphinium", " Anemone "],
            answer: 1
        }
    ],

    //step3
    index:0,
    //step4

    show:function(){
        if(this.index<= this.questions.length -1){

            quizImage.innerHTML = '<img src=" '+this.questions[this.index].image + ' " />';
    
            quizQuestions.innerHTML = this.questions[this.index].question;
            option1.innerHTML = this.questions[this.index].options[0];
            option2.innerHTML = this.questions[this.index].options[1];
            option3.innerHTML =this.questions[this.index].options[2];
            option4.innerHTML = this.questions[this.index].options[3];
            this.scoreCard();
           
        }else{

            quizQuestions.innerHTML = "You just finished the quiz your total score is";
            quizImage.style.display = "none";
    
            quizQuestions.style.display = "none";
            option1.style.display = "none";
            option2.style.display = "none";
            option3.style.display ="none";
            option4.style.display = "none";

        }



    },
    //step7
    check:function (ele) {
        let id = ele.id;

        if(id[id.length-1] == this.questions[this.index].answer){
            this.score++;

           ele.className = "correct";
           ele.innerHTML = "Correct";
           this.scoreCard();
        }else{
            ele.className= "incorrect";
            ele.innerHTML="Incorrect";
        }
       
        
    },

    //step8
    score:0,
    scoreCard:function(){
        scoreCard.innerHTML = this.questions.length + "/"+this.score;

    },

    //step10
    notChecked:function () {
        for(let i=0; i<ul.children.length; i++){
            //si les autres options ne sont pas été vérefies on empeche le cursor de se cliquer
            ul.children[i].style.pointerEvents= "none";
        }
        


        
    }, 

    checked:function () {

        for(let i=0; i<ul.children.length; i++){
            ul.children[i].style.pointerEvents = "auto";
            ul.children[i].className = '';
        }
        
    },

    next:function(){
        this.index++;
        this.show();

    },





  

}


//step5
window.onload = quiz.show();
//step6
function  button(ele) {
quiz.check(ele);
//step9
quiz.notChecked();

    
}

function next() {
    quiz.next();
    quiz.checked();
    
}